package at.MaxPlays.OneLine.arena;

import java.io.Serializable;

import org.bukkit.Location;

import at.MaxPlays.OneLine.util.SerializableLocation;

/**
* Created by Max_Plays on 07.06.2017
*/
public class Arena implements Serializable{

	private static final long serialVersionUID = -911309619387377709L;
	private SerializableLocation spawn1, spawn2;
	private final int loy;
	private boolean active = false;
	private String name;
	
	public Arena(String name, Location spawn1, Location spawn2){
		this.spawn1 = new SerializableLocation(spawn1);
		this.spawn2 = new SerializableLocation(spawn2);
		this.name = name;
		loy = Math.min(spawn1.getBlockY(), spawn2.getBlockY());
	}
	
	public void setActive(boolean active){
		this.active = active;
	}
	public boolean isActive(){
		return active;
	}

	public SerializableLocation getSpawn1() {
		return spawn1;
	}

	public SerializableLocation getSpawn2() {
		return spawn2;
	}

	public int getLoy() {
		return loy;
	}
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
}
