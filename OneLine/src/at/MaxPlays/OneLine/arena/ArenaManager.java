package at.MaxPlays.OneLine.arena;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.bukkit.Location;

import at.MaxPlays.OneLine.game.GameManager;

/**
* Created by Max_Plays on 07.06.2017
*/
public class ArenaManager {

	public static ArrayList<Arena> arenas = new ArrayList<>();
	public static final File arenaFile = new File("plugins/OneLine/arenas.dat");
	
	public static Arena getNewArena(String name, Location spawn1, Location spawn2){
		Arena a = new Arena(name, spawn1, spawn2);
		arenas.add(a);
		return a;
	}
	public static boolean deleteArena(Arena a){
		if(arenas.contains(a)){
			if(a.isActive())
				GameManager.endGame(a);
			arenas.remove(a);
			return true;
		}
		return false;
	}
	public static void save(){
		
		for(Arena a: arenas)
			a.setActive(false);
		
		try{
			if(arenaFile.exists())
				arenaFile.delete();
			FileOutputStream fos = new FileOutputStream(arenaFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(arenas);
			oos.flush();
			fos.flush();
			oos.close();
			fos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	@SuppressWarnings("unchecked")
	public static void load(){
		try{
			FileInputStream fis = new FileInputStream(arenaFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			arenas = (ArrayList<Arena>) ois.readObject();
			ois.close();
			fis.close();
		}catch(Exception e){
			//No arenas
		}
	}
	public static Arena getAvailableArena(){
		for(Arena a: arenas){
			if(!a.isActive())
				return a;
		}
		return null;
	}
	
}
