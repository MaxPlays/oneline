package at.MaxPlays.OneLine.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Cuboid implements Iterable<Block>{

private int lox, hix, loy, hiy, loz, hiz;
private World world;

//TODO Insert declarations

public Cuboid(int x, int y, int z, int dx, int dy, int dz, World w){

	calculateLowHigh(x, y, z, dx, dy, dz);
	this.world = w;  

}

public Cuboid(Location loc1, Location loc2, String world){

	
	
	this.world = Bukkit.getWorld(world);
	calculateLowHigh(loc1.getBlockX(), loc1.getBlockY(), loc1.getBlockZ(),
			 loc2.getBlockX(), loc2.getBlockY(), loc2.getBlockZ());	

}

public Cuboid(int lox, int loy, int loz, int hix, int hiy, int hiz, boolean putSomething, World w){

  	this.lox = lox;
	this.loy = loy;
	this.loz = loz;
	this.hix = hix;
	this.hiy = hiy;
  	this.hiz = hiz;

	this.world = w;

	if(putSomething != false){
		//Do nothing
	}
	
	

}

public boolean inCuboid(Location loc){
	
	return loc.toVector().isInAABB(new Vector(lox, loy, loz), new Vector(hix + 1, hiy, hiz + 1));
}

public boolean inCuboid(Player p){
	
	return p.getLocation().toVector().isInAABB(new Vector(lox, loy, loz), new Vector(hix + 1, hiy, hiz + 1));
}

public String toString(){

  return lox + ":" + loy + ":" + loz + ":" + hix + ":" + hiy + ":" + hiz + ":" + world.getName();

}

public static Cuboid fromString(String s){

  	String[] array = s.split(":");
	return new Cuboid(Integer.valueOf(array[0]), Integer.valueOf(array[1]), Integer.valueOf(array[2])
                                 , Integer.valueOf(array[3]), Integer.valueOf(array[4]), Integer.valueOf(array[5]), true, Bukkit.getWorld(array[6]));
}

private void calculateLowHigh(int x, int y, int z, int dx, int dy, int dz){

	lox = Math.min(x, dx);
	loy = Math.min(y, dy);
	loz = Math.min(z, dz);
	hix = Math.max(x, dx);
	hiy = Math.max(y, dy);
	hiz = Math.max(z, dz);
	

}
@Override
public Iterator<Block> iterator() {
        return getBlocks().iterator();
}

private List<Block> getBlocks(){
	List<Block> list = new ArrayList<>();
	for(int x = lox; x <= hix; x++){
		for(int y = loy; y <= hiy; y++){
			for(int z = loz; z <= hiz; z++){
				list.add(world.getBlockAt(x, y, z));	
			}
		}
	}
return list;
}
}