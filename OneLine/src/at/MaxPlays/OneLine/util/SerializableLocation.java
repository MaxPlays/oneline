package at.MaxPlays.OneLine.util;

import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
* Created by Max_Plays on 07.06.2017
*/
public class SerializableLocation implements Serializable{

	private static final long serialVersionUID = 6748318724802984084L;

	private int x, y, z;
	private float yaw, pitch;
	private final String world;
	
	public SerializableLocation(Location loc){
		x = loc.getBlockX();
		y = loc.getBlockY();
		z = loc.getBlockZ();
		this.yaw = loc.getYaw();
		this.pitch = loc.getPitch();
		world = loc.getWorld().getName();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}
	
	public Location getLocation(){
		Location loc = new Location(Bukkit.getWorld(world), x + .5, y, z+ .5);
		loc.setYaw(yaw);
		loc.setPitch(pitch);
		return loc;
	}
	
}
