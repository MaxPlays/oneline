package at.MaxPlays.OneLine.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

public class Stack {

	private ItemStack is;
	private ItemMeta m;
	
	public Stack(Material m, int amount, int subid){
		is = new ItemStack(m, amount, (short) subid);
		this.m = is.getItemMeta();
	}
	public Stack(Material m, int amount){
		is = new ItemStack(m, amount);
		this.m = is.getItemMeta();
	}
	public Stack(Material m){
		is = new ItemStack(m, 1);
		this.m = is.getItemMeta();
	}
	public Stack(ItemStack s){
		int amount = s.getAmount();
		MaterialData d = s.getData();
		short durability = s.getDurability();
		ItemMeta im = s.getItemMeta();
		Material mat = s.getType();
		
		ItemStack as = new ItemStack(mat, amount);
		as.setDurability(durability);
		as.setItemMeta(im);
		as.setData(d);
		m = im;
		
		is = as;
	}


	public Stack setLore(String line1, String line2, String line3){
		m.setLore(buildLore(line1, line2, line3));
		return this;
	}
	public Stack setName(String name){
		m.setDisplayName(name);
		return this;
	}

	public Stack addEnchantment(Enchantment e, int lvl){
		m.addEnchant(e, lvl, true);
		return this;
	}
	public Stack setAmount(int amount){
		is.setItemMeta(m);
		is.setAmount(amount);
		m = is.getItemMeta();
		return this;
	}
	public ItemStack build(){
		is.setItemMeta(m);
		ItemStack as = is;
		as.setItemMeta(m);
		is = null;
		m = null;
		return as;
	}
	private List<String> buildLore(String line1, String line2, String line3){

		List<String> lore = new ArrayList<String>();
		
		if(line1 != null){
			lore.add(line1);
		}
		if(line2 != null){
			lore.add(line2);
		}
		if(line3 != null){
			lore.add(line3);
		}
		
		return lore;
	}


	public Stack addGlow() {

		is.setItemMeta(m);
		
		net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(is);
		NBTTagCompound tag = null;
		if (!nmsStack.hasTag()) {
			tag = new NBTTagCompound();
			nmsStack.setTag(tag);
		}
		if (tag == null) tag = nmsStack.getTag();
		NBTTagList ench = new NBTTagList();
		tag.set("ench", ench);
		nmsStack.setTag(tag);
		is = CraftItemStack.asCraftMirror(nmsStack);
		
		m = is.getItemMeta();
		
		return this;

	}
}