package at.MaxPlays.OneLine.command;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.MaxPlays.OneLine.Player.PlayerManager;
import at.MaxPlays.OneLine.Queue.QueueManager;
import at.MaxPlays.OneLine.arena.Arena;
import at.MaxPlays.OneLine.arena.ArenaManager;
import at.MaxPlays.OneLine.editor.ArenaEditor;
import at.MaxPlays.OneLine.editor.RegionEditor;
import at.MaxPlays.OneLine.game.Game;
import at.MaxPlays.OneLine.game.GameState;
import at.MaxPlays.OneLine.main.Config;
import at.MaxPlays.OneLine.main.OneLine;

/**
* Created by Max_Plays on 07.06.2017
*/
public class CommandOneLine implements CommandExecutor {

	
	public static HashMap<Player, Integer> scheds = new HashMap<>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player && cmd.getName().equalsIgnoreCase("oneline")){
			final Player p = (Player) sender;
			
			if(args.length == 0){
				sendHelp(p);
			}else if(args.length == 1){
				if(args[0].equalsIgnoreCase("arenas") && p.hasPermission("OneLine.*")){
					
					if(ArenaManager.arenas.size() > 0){
						p.sendMessage(OneLine.prefix + "Es sind �8" + ArenaManager.arenas.size() + " �7Arenen registriert");
						for(Arena a: ArenaManager.arenas){
							p.sendMessage("�8- �7" + a.getName());
						}
					}else{
						p.sendMessage(OneLine.prefix + "Es sind noch keine Arenen vorhanden");
					}
					
				}else if(args[0].equalsIgnoreCase("addspawn") && p.hasPermission("OneLine.*")){
					
					if(ArenaEditor.isEditing(p)){
						ArenaEditor.getEditor(p).registerSpawn(p.getLocation());
					}else{
						p.sendMessage(OneLine.prefix + "Du befindest dich nicht im Bearbeitungsmodus. Erstelle eine neue Arena mit �c�o/oneline addarena <Name>");
					}
					
				}else if(args[0].equalsIgnoreCase("surrender")){
				
					if(PlayerManager.getStatus(p).equals(GameState.INGAME)){
						Game g = PlayerManager.getGame(p);
						g.finish(g.getOpponent(p), p, 0);
					}else{
						p.sendMessage(OneLine.prefix + "Du bist in keine Spiel");
					}
						
				}else if(args[0].equalsIgnoreCase("setregion") && p.hasPermission("OneLine.*")){
						
					if(RegionEditor.editing == null){
						RegionEditor.editing = p.getName();
						p.sendMessage(OneLine.prefix + "Du bearbeitest nun die Challenge-Region. Schlage den 1. Basisblock deiner Region");
						Bukkit.getScheduler().runTaskLater(OneLine.instance, new Runnable() {
							
							@Override
							public void run() {
								if(RegionEditor.editing != null && RegionEditor.editing.equals(p)){
									RegionEditor.editing = null;
									RegionEditor.a = null;
									p.sendMessage(OneLine.prefix + "Du verl�sst den Bearbeitungsmodus");
								}
							}
						}, 20*60);
					}else{
						p.sendMessage(OneLine.prefix + "Es bearbeitet bereits jemand die Challenge-Region");
					}
						
				}else{
					sendHelp(p);
				}
			}else if(args.length == 2){
				String arg = args[1];
				if(args[0].equalsIgnoreCase("challenge")){
					
					if(Bukkit.getPlayer(arg) == null){
						p.sendMessage(OneLine.prefix + "Der Spieler ist nicht online");
						return true;
					}
					Player hit = Bukkit.getPlayer(arg);
					
					if(Config.cuboid.inCuboid(hit) && Config.cuboid.inCuboid(p)){
							if(PlayerManager.hasRequested(hit, p)){
								
								PlayerManager.revokeRequest(hit);
								QueueManager.enterQueue(hit, p);
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 2);
								hit.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 2);
								hit.sendMessage(OneLine.prefix + "�6" + p.getName() + " �7hat deine Herausforderung angenommen. Ihr werdet so bald als m�glich in eine freie Arena teleportiert");
								p.sendMessage(OneLine.prefix + "Du hast die Herausforderunmg angenommen. Ihr werdet so bald als m�lich in eine freie Arena teleportiert");
								
							}else if(PlayerManager.hasRequested(p, hit)){				
								p.sendMessage(OneLine.prefix + "Du hast diesen Spieler bereits herausgefordert");
							}else{
							
								PlayerManager.setRequested(p, hit);
								p.sendMessage(OneLine.prefix + "Du hast �6" + hit.getName() + " �7herausgefordert");
								hit.sendMessage(OneLine.prefix + "Du wurdest von �6" + p.getName() + " �7herausgefordert");
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 2);
								hit.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 2);
							}
						
					}else{
						p.sendMessage(OneLine.prefix + "Der Spieler befindet sich nicht in der Challenge-Zone");
					}
					
				}else if(args[0].equalsIgnoreCase("addarena") && p.hasPermission("OneLine.*")){
					
					if(!ArenaEditor.isEditing(p)){
						new ArenaEditor(p, arg);
						p.sendMessage(OneLine.prefix + "Du berarbeitest nun die Arena �c" + arg + "�7. F�ge den ersten Spawn mit �c�o/oneline addspawn �7hinzu");
					
						int i = Bukkit.getScheduler().scheduleSyncDelayedTask(OneLine.instance, new Runnable() {
							
							@Override
							public void run() {
								ArenaEditor.abort(p);
								p.sendMessage(OneLine.prefix + "Du bearbeitest die Arena nicht mehr");
							}
						}, 20*60*2);
						cancelSched(p);
						scheds.put(p, i);
					
					}else{
						p.sendMessage(OneLine.prefix + "Du bearbeitest bereits eine Arena");
					}
					
				}else if(args[0].equalsIgnoreCase("delarena") && p.hasPermission("OneLine.*")){
					
					for(Arena a: ArenaManager.arenas){
						if(a.getName().toLowerCase().equals(arg.toLowerCase()))
							ArenaManager.deleteArena(a);
					}
					p.sendMessage(OneLine.prefix + "Der Command wurde erfolgreich ausgef�hrt");
					
				}else{
					sendHelp(p);
				}
			}else{
				sendHelp(p);
			}
			
			
		}
		
		return true;
	}
	private void sendHelp(Player p){
		p.sendMessage("�8-------------- �7[�cOneLine�7] �8--------------");
		p.sendMessage("�c/oneline challenge <Spieler> �7Fordere einen Spieler heraus");
		p.sendMessage("�c/oneline surrender �7Gebe das aktuelle Spiel auf");
		if(p.hasPermission("OneLine.*")){
			p.sendMessage("�c/oneline setregion �7Setze die Challenge-Region");
			p.sendMessage("�c/oneline addarena <Name> �7F�ge eine Arena hinzu");
			p.sendMessage("�c/oneline addspawn �7Setze einen neuen Spawn");
			p.sendMessage("�c/oneline arenas �7Liste aller Arenen");
			p.sendMessage("�c/oneline delarena <arena> �7L�sche eine Arena");
		}
		p.sendMessage("�8----------------------------------------");
	}
	public static void cancelSched(Player p){
		if(scheds.containsKey(p)){
			Bukkit.getScheduler().cancelTask(scheds.get(p));
			scheds.remove(p);
		}
	}

}
