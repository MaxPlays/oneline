package at.MaxPlays.OneLine.Player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import at.MaxPlays.OneLine.main.Config;

/**
* Created by Max_Plays on 08.06.2017
*/
public class SignListener implements Listener {

	@EventHandler
	public void onSign(SignChangeEvent e){
		Player p = e.getPlayer();
		if(p.hasPermission("OneLine.*")){
			if(e.getLine(0).equalsIgnoreCase("") && e.getLine(1).equalsIgnoreCase("[Warteschlange]") && e.getLine(2).equalsIgnoreCase("OneLine") && e.getLine(3).equalsIgnoreCase("") && Config.cuboid.inCuboid(e.getBlock().getLocation())){
				e.setLine(1, "�6Warteschlange");
				e.setLine(2, "�7OneLine");
			}
		}
	}
	
}
