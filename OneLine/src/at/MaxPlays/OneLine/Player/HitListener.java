package at.MaxPlays.OneLine.Player;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import at.MaxPlays.OneLine.Queue.QueueManager;
import at.MaxPlays.OneLine.main.Config;
import at.MaxPlays.OneLine.main.OneLine;
import at.MaxPlays.OneLine.util.Stack;

/**
* Created by Max_Plays on 07.06.2017
*/
public class HitListener implements Listener{

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player && e.getEntity() instanceof Player){
			Player hitter = (Player) e.getDamager();
			Player hit = (Player) e.getEntity();
			
			if(Config.cuboid.inCuboid(hit) && Config.cuboid.inCuboid(hitter)){
				if(hitter.getItemInHand() != null && hitter.getItemInHand().getType().equals(Material.DIAMOND_SWORD)){
					e.setCancelled(true);
					hitter.getInventory().setItem(0, new Stack(Material.DIAMOND_SWORD).setName("�aHerausfordern").build());
					
					if(PlayerManager.hasRequested(hit, hitter)){
						
						PlayerManager.revokeRequest(hit);
						QueueManager.enterQueue(hit, hitter);
						hitter.playSound(hitter.getLocation(), Sound.ORB_PICKUP, 1, 2);
						hit.playSound(hitter.getLocation(), Sound.ORB_PICKUP, 1, 2);
						hit.sendMessage(OneLine.prefix + "�6" + hitter.getName() + " �7hat deine Herausforderung angenommen. Ihr werdet so bald wie m�glich in eine freie Arena teleportiert");
						hitter.sendMessage(OneLine.prefix + "Du hast die Herausforderunmg angenommen. Ihr werdet so bald wie m�glich in eine freie Arena teleportiert");
						
					}else if(PlayerManager.hasRequested(hitter, hit)){				
						hitter.sendMessage(OneLine.prefix + "Du hast diesen Spieler bereits herausgefordert");
					}else{
					
						PlayerManager.setRequested(hitter, hit);
						hitter.sendMessage(OneLine.prefix + "Du hast �6" + hit.getName() + " �7herausgefordert");
						hit.sendMessage(OneLine.prefix + "Du wurdest von �6" + hitter.getName() + " �7herausgefordert");
						hitter.playSound(hitter.getLocation(), Sound.ORB_PICKUP, 1, 2);
						hit.playSound(hitter.getLocation(), Sound.ORB_PICKUP, 1, 2);
					}
				}
			}
			
		}
	}
	
}
