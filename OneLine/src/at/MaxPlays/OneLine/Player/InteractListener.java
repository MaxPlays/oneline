package at.MaxPlays.OneLine.Player;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import at.MaxPlays.OneLine.Queue.QueueManager;
import at.MaxPlays.OneLine.main.Config;

/**
* Created by Max_Plays on 07.06.2017
*/
public class InteractListener implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK) | e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
			if(e.getClickedBlock().getType().equals(Material.WALL_SIGN) | e.getClickedBlock().getType().equals(Material.SIGN) | e.getClickedBlock().getType().equals(Material.SIGN_POST)){
				Sign s = (Sign) e.getClickedBlock().getState();
				if(s.getLine(1).equals("�6Warteschlange") && s.getLine(2).equals("�7OneLine") && Config.cuboid.inCuboid(p)){
					e.setCancelled(true);
					QueueManager.registerClick(p);
				}
			}
		}
	}
	
}
