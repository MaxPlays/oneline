package at.MaxPlays.OneLine.Player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import at.MaxPlays.OneLine.Queue.QueueManager;
import at.MaxPlays.OneLine.editor.ArenaEditor;
import at.MaxPlays.OneLine.game.GameState;
import at.MaxPlays.OneLine.main.Config;

/**
* Created by Max_Plays on 07.06.2017
*/
public class QuitListener implements Listener{

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		ArenaEditor.abort(p);
		MoveListener.lobby.remove(p);
		if(PlayerManager.getStatus(p) != GameState.NONE){
			if(PlayerManager.getStatus(p) == GameState.QUEUED){
				QueueManager.leaveQueue(p);
			}else{
				PlayerManager.getGame(p).end();
			}
		}else{
			if(Config.cuboid.inCuboid(p)){
				MoveListener.loadInv(p);
				p.teleport(p.getWorld().getSpawnLocation());
			}
		}
	}
	
}
