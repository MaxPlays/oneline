package at.MaxPlays.OneLine.Player;

import java.util.HashMap;

import org.bukkit.entity.Player;

import at.MaxPlays.OneLine.game.Game;
import at.MaxPlays.OneLine.game.GameManager;
import at.MaxPlays.OneLine.game.GameState;

/**
* Created by Max_Plays on 07.06.2017
*/
public class PlayerManager {

	public static HashMap<Player, GameState> players = new HashMap<>();
	public static HashMap<Player, Game> games = new HashMap<>();
	public static HashMap<Player, Player> requests = new HashMap<>();
	
	public static GameState getStatus(Player p){
		if(players.containsKey(p))
			return players.get(p);
		return GameState.NONE;
	}
	public static void setStatus(Player p, GameState state){
		if(players.containsKey(p))
			players.remove(p);
		players.put(p, state);
	}
	public static void endGame(Player p){
		if(games.containsKey(p)){
			GameManager.endGame(games.get(p));
			games.remove(p);
			setStatus(p, GameState.NONE);
		}
	}
	public static void setGame(Player p, Game g){
		endGame(p);
		players.put(p, GameState.INGAME);
		games.put(p, g);
	}
	public static Game getGame(Player p){
		if(games.containsKey(p))
			return games.get(p);
		return null;
	}
	public static void revokeRequest(Player p){
		if(requests.containsKey(p))
			requests.remove(p);
	}
	public static void setRequested(Player requester, Player requested){
		revokeRequest(requester);
		requests.put(requester, requested);
	}
	public static boolean hasRequested(Player who, Player whom){
		if(requests.containsKey(who) && requests.get(who).equals(whom))
			return true;
		return false;
	}
	
}
