package at.MaxPlays.OneLine.Player;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import at.MaxPlays.OneLine.game.GameState;
import at.MaxPlays.OneLine.main.OneLine;

/**
* Created by Max_Plays on 07.06.2017
*/
public class BlockListener implements Listener {

	@EventHandler
	public void onPlace(final BlockPlaceEvent e){
		Player p = e.getPlayer();
		if(PlayerManager.getStatus(p).equals(GameState.INGAME)){
			Bukkit.getScheduler().runTaskLater(OneLine.instance, new Runnable() {
				
				@Override
				public void run() {
					e.getBlock().setType(Material.REDSTONE_BLOCK);
					Bukkit.getScheduler().runTaskLater(OneLine.instance, new Runnable() {
						
						@Override
						public void run() {
							e.getBlock().setType(Material.AIR);
						}
					}, 20);
				}
			}, 40);
		}
	}
	
}
