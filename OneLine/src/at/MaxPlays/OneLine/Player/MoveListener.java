package at.MaxPlays.OneLine.Player;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import at.MaxPlays.OneLine.Queue.QueueManager;
import at.MaxPlays.OneLine.game.Game;
import at.MaxPlays.OneLine.game.GameState;
import at.MaxPlays.OneLine.main.Config;
import at.MaxPlays.OneLine.util.Stack;

/**
* Created by Max_Plays on 07.06.2017
*/
public class MoveListener implements Listener {

	public static HashMap<Player, ItemStack[]> inv = new HashMap<>();
	public static ArrayList<Player> lobby = new ArrayList<>();
	
	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(PlayerManager.getStatus(p).equals(GameState.INGAME)){
			Game g = PlayerManager.getGame(p);
			if(e.getTo().getBlockY() < g.getArena().getLoy() - 5){
				g.score(g.getOpponent(p));
				p.playSound(p.getLocation(), Sound.ENDERDRAGON_HIT, 1, 1);
				g.getOpponent(p).playSound(g.getOpponent(p).getLocation(), Sound.ORB_PICKUP, 1, 1);
			}
		}else{
			if(Config.cuboid.inCuboid(p)){
				if(!lobby.contains(p)){
					lobby.add(p);
					storeInv(p);
					p.setGameMode(GameMode.SURVIVAL);
					p.getInventory().setItem(0, new Stack(Material.DIAMOND_SWORD).setName("žaHerausfordern").build());
					p.playSound(p.getLocation(), Sound.NOTE_BASS, 1, 1);
				}
			}else{
				if(lobby.contains(p)){
					lobby.remove(p);
					if(!PlayerManager.getStatus(p).equals(GameState.INGAME))
						loadInv(p);
					p.playSound(p.getLocation(), Sound.NOTE_BASS, 1, 1);
					QueueManager.leaveQueue(p);
				}
			}
		}
	}
	@EventHandler
	public void onCmd(PlayerCommandPreprocessEvent e){
		String[] msg = e.getMessage().split(" ");
		Player p = e.getPlayer();
		if(msg.length == 1 && msg[0].replace("/", "").equalsIgnoreCase("hub") && PlayerManager.getStatus(p).equals(GameState.INGAME)){
			e.setCancelled(true);
			Game g = PlayerManager.getGame(p);
			g.finish(g.getOpponent(p), p, 0);
		}
	}
	@EventHandler
	public void onInv(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		ItemStack i = e.getCurrentItem();
		Inventory inv = e.getInventory();
		if(inv != null && i != null){
			if(PlayerManager.getStatus(p).equals(GameState.INGAME) | lobby.contains(p)){
				e.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		if(PlayerManager.getStatus(p).equals(GameState.INGAME) | lobby.contains(p)){
			e.setCancelled(true);
		}
	}
	
	public static void storeInv(Player p){
		if(inv.containsKey(p))
			inv.remove(p);
		inv.put(p, p.getInventory().getContents());
		p.getInventory().clear();
	}
	public static void loadInv(Player p){
		if(inv.containsKey(p)){
			p.getInventory().setContents(inv.get(p));
			inv.remove(p);
		}
	}
	
}
