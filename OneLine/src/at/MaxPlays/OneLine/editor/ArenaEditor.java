package at.MaxPlays.OneLine.editor;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import at.MaxPlays.OneLine.arena.ArenaManager;
import at.MaxPlays.OneLine.command.CommandOneLine;
import at.MaxPlays.OneLine.main.OneLine;

public class ArenaEditor {

	static ArrayList<ArenaEditor> editors = new ArrayList<>();
	private Player p;
	private String name;
	private Location spawn1;
	
	public ArenaEditor(Player p, String name){
		this.p = p;
		this.name = name;
		editors.add(this);
	}
	private void setSpawn1(Location loc){
		this.spawn1 = loc;
	}
	private void setSpawn2(Location loc){
		if(spawn1 != null){
			ArenaManager.getNewArena(name, spawn1, loc);
			editors.remove(this);
		}
	}
	public static void abort(Player p){
		CommandOneLine.cancelSched(p);
		for(ArenaEditor e: editors){
			if(e.getPlayer().equals(p)){
				editors.remove(e);
			}
		}
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Player getPlayer() {
		return p;
	}
	public Location getSpawn1() {
		return spawn1;
	}
	public static boolean isEditing(Player p){
		for(ArenaEditor e: editors){
			if(e.getPlayer().equals(p))
				return true;
		}
		return false;
	}
	public void registerSpawn(Location loc){
		if(spawn1 == null){
			setSpawn1(loc);
			p.sendMessage(OneLine.prefix + "Du hast den 1. Spawn gesetzt");
			return;
		}else{
			setSpawn2(loc);
			p.sendMessage(OneLine.prefix + "Die Arena �c" + name + " �7wurde erstellt");
		}
	}
	public static ArenaEditor getEditor(Player p){
		for(ArenaEditor e: editors){
			if(e.getPlayer().equals(p))
				return e;
		}
		return null;
	}
}
