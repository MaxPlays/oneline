package at.MaxPlays.OneLine.editor;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import at.MaxPlays.OneLine.main.Config;
import at.MaxPlays.OneLine.main.OneLine;
import at.MaxPlays.OneLine.util.Cuboid;

/**
* Created by Max_Plays on 07.06.2017
*/
public class RegionEditor implements Listener{

	public static String editing;
	public static Location a;
	
	@EventHandler
	public void onBreak(BlockBreakEvent e){
		Player p = e.getPlayer();
		if(p.getName().equals(editing)){
			e.setCancelled(true);
			if(a == null){
				a = e.getBlock().getLocation();
				p.sendMessage(OneLine.prefix + "Die erste Position wurde gesetzt");
			}else{
				Config.cuboid = new Cuboid(a, e.getBlock().getLocation(), e.getBlock().getWorld().getName());
				a = null;
				editing = null;
				p.sendMessage(OneLine.prefix + "Du hast die neue Challenge-Region gesetzt");
			}
		}
	}
	
}
