package at.MaxPlays.OneLine.Queue;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import at.MaxPlays.OneLine.Player.PlayerManager;
import at.MaxPlays.OneLine.arena.Arena;
import at.MaxPlays.OneLine.arena.ArenaManager;
import at.MaxPlays.OneLine.game.GameManager;
import at.MaxPlays.OneLine.game.GameState;
import at.MaxPlays.OneLine.main.OneLine;

/**
* Created by Max_Plays on 07.06.2017
*/
public class QueueManager {

	public static ArrayList<QueueEntry> queue = new ArrayList<>();
	public static Player p;
	
	public QueueManager(){
		Bukkit.getScheduler().runTaskTimer(OneLine.instance, new Runnable() {
			
			@Override
			public void run() {
				if(waiting()){
					Arena a = ArenaManager.getAvailableArena();
					if(a != null){
						QueueEntry e = getTop();
						GameManager.getNewGame(e.getP1(), e.getP2(), a);
					}
				}
			}
		}, 20, 20);
	}
	
	public static void enterQueue(Player p1, Player p2){
		PlayerManager.setStatus(p1, GameState.QUEUED);
		PlayerManager.setStatus(p2, GameState.QUEUED);
		queue.add(new QueueEntry(p1, p2));
	}
	public static boolean waiting(){
		return queue.size() > 0;
	}
	public static QueueEntry getTop(){
		QueueEntry e = queue.get(0);
		queue.remove(e);
		return e;
	}
	public static void registerClick(Player player){
		if(p == null){
			p = player;
			PlayerManager.setStatus(player, GameState.QUEUED);
			player.sendMessage(OneLine.prefix + "Du betrittst die Warteschlange");
			player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
		}else{
			
			if(p != null && p.equals(player)){
				p = null;
				player.sendMessage(OneLine.prefix + "Du verl�sst die Warteschlange");
				player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
				PlayerManager.setStatus(player, GameState.NONE);
				return;
			}
				player.sendMessage(OneLine.prefix + "Dein Spiel beginnt, sobald eine Arena frei wird");
				p.sendMessage(OneLine.prefix + "Dein Spiel beginnt, sobald eine Arena frei wird");
				enterQueue(p, player);
				p = null;
		}
		
	}
	public static void leaveQueue(Player player){
		if(p != null && p.equals(player)){
			p = null;
			player.sendMessage(OneLine.prefix + "Du verl�sst die Warteschlange");
			player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
		}
		PlayerManager.setStatus(player, GameState.NONE);
		for(QueueEntry e: queue){
			if(e.getP1().equals(player) | e.getP2().equals(player)){
				PlayerManager.setStatus(e.getP1(), GameState.NONE);
				PlayerManager.setStatus(e.getP2(), GameState.NONE);
				break;
			}
		}
	}
	
}
