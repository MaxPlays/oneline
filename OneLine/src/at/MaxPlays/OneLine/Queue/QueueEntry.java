package at.MaxPlays.OneLine.Queue;

import org.bukkit.entity.Player;

/**
* Created by Max_Plays on 07.06.2017
*/
public class QueueEntry {

	private Player p1, p2;

	public QueueEntry(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
	}

	public Player getP1() {
		return p1;
	}

	public Player getP2() {
		return p2;
	}
}
