package at.MaxPlays.OneLine.main;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import at.MaxPlays.OneLine.Player.BlockListener;
import at.MaxPlays.OneLine.Player.HitListener;
import at.MaxPlays.OneLine.Player.InteractListener;
import at.MaxPlays.OneLine.Player.MoveListener;
import at.MaxPlays.OneLine.Player.QuitListener;
import at.MaxPlays.OneLine.Player.SignListener;
import at.MaxPlays.OneLine.Queue.QueueManager;
import at.MaxPlays.OneLine.arena.ArenaManager;
import at.MaxPlays.OneLine.command.CommandOneLine;
import at.MaxPlays.OneLine.editor.RegionEditor;
import at.MaxPlays.OneLine.game.Game;
import at.MaxPlays.OneLine.game.GameManager;

/**
* Created by Max_Plays on 07.06.2017
*/
public class OneLine extends JavaPlugin{

	public static final String prefix = "§8▎ §cOneLine §8» §7";
	public static OneLine instance = null;
	public static File spawns;
	public static FileConfiguration cfg;
	
	public void onEnable(){
		
		instance = this;
		ArenaManager.load();
		new QueueManager();
		
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new QuitListener(), this);
		pm.registerEvents(new MoveListener(), this);
		pm.registerEvents(new InteractListener(), this);
		pm.registerEvents(new BlockListener(), this);
		pm.registerEvents(new RegionEditor(), this);
		pm.registerEvents(new HitListener(), this);
		pm.registerEvents(new SignListener(), this);
		
		getCommand("oneline").setExecutor(new CommandOneLine());
		
		spawns = new File("plugins/OneLine/config.dat");
		cfg = YamlConfiguration.loadConfiguration(spawns);
		cfg.options().copyDefaults(true);
		try {
			cfg.save(spawns);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Config.loadConfig();
		
	}
	public void onDisable(){
		for(Game g: GameManager.games)
			g.endSoft();
		GameManager.games.clear();
		ArenaManager.save();
		Config.saveConfig();
		for(Player p: Bukkit.getOnlinePlayers()){
			if(Config.cuboid.inCuboid(p)){
				MoveListener.loadInv(p);
				p.teleport(p.getWorld().getSpawnLocation());
			}
		}
	}
	
}
