package at.MaxPlays.OneLine.main;

import java.io.IOException;

import org.bukkit.Bukkit;

import at.MaxPlays.OneLine.util.Cuboid;

/**
* Created by Max_Plays on 07.06.2017
*/
public class Config {

	public static Cuboid cuboid = new Cuboid(0, 0, 0, 1, 1, 1, Bukkit.getWorld("world_nether"));;
	
	public static void loadConfig(){
		if(OneLine.cfg.get("region") != null)
			cuboid = Cuboid.fromString(OneLine.cfg.getString("region"));
		
	}
	public static void saveConfig(){
		OneLine.cfg.set("region", cuboid.toString());
		try {
			OneLine.cfg.save(OneLine.spawns);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
