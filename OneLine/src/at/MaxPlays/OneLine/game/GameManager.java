package at.MaxPlays.OneLine.game;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import at.MaxPlays.OneLine.arena.Arena;

/**
* Created by Max_Plays on 07.06.2017
*/
public class GameManager {

	public static ArrayList<Game> games = new ArrayList<>();
	
	public static Game getNewGame(Player p1, Player p2, Arena a){
		Game g = new Game(p1, p2, a);
		games.add(g);
		return g;
	}
	
	public static void endGame(Game g){
		g.end();
		games.remove(g);
	}
	public void endGameSoft(Game g){
		g.endSoft();
	}
	public static void endGame(Arena a){
		for(Game g: GameManager.games){
			if(g.getArena().equals(a)){
				g.end();
				games.remove(g);
			}
		}
	}
	
}
