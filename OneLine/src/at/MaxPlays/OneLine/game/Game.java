package at.MaxPlays.OneLine.game;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import at.MaxPlays.OneLine.Player.PlayerManager;
import at.MaxPlays.OneLine.arena.Arena;
import at.MaxPlays.OneLine.main.OneLine;
import at.MaxPlays.OneLine.util.Stack;
import at.MaxPlays.OneLine.util.Title;

/**
* Created by Max_Plays on 07.06.2017
*/
public class Game {

	private Player p1, p2;
	private ItemStack[] inv1, inv2, arm1, arm2;
	private Location loc1, loc2;
	private Arena a;
	private int score1 = 0, score2 = 0;
	
	public Game(Player p1, Player p2, Arena a){
		
		a.setActive(true);
		
		this.p1 = p1;
		this.p2 = p2;
		this.a = a;
		
		this.loc1 = p1.getLocation();
		this.loc2 = p2.getLocation();
		
		inv1 = p1.getInventory().getContents();
		inv2 = p2.getInventory().getContents();
		arm1 = p1.getInventory().getArmorContents();
		arm2 = p2.getInventory().getArmorContents();
		
		p1.getInventory().clear();
		p1.getInventory().setArmorContents(null);
		p2.getInventory().clear();
		p2.getInventory().setArmorContents(null);
		
		p1.teleport(a.getSpawn1().getLocation());
		p2.teleport(a.getSpawn2().getLocation());
		
		PlayerManager.setStatus(p1, GameState.INGAME);
		PlayerManager.setStatus(p2, GameState.INGAME);
		
		PlayerManager.setGame(p1, this);
		PlayerManager.setGame(p2, this);
		
		p1.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20*100000, 254));
		p2.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20*100000, 254));
		
		p1.playSound(p1.getLocation(), Sound.LEVEL_UP, 1, 1);
		p2.playSound(p2.getLocation(), Sound.LEVEL_UP, 1, 1);
		
		inv();
		
	}
	public void broadcast(String msg){
		p1.sendMessage(OneLine.prefix + msg);
		p2.sendMessage(OneLine.prefix + msg);
	}
	public void end(){
		a.setActive(false);
		p1.getInventory().setContents(inv1);
		p2.getInventory().setContents(inv2);
		p1.getInventory().setArmorContents(arm1);
		p2.getInventory().setArmorContents(arm2);
		
    	teleport();
		
		GameManager.games.remove(this);
		
		PlayerManager.setStatus(p1, GameState.NONE);
		PlayerManager.setStatus(p2, GameState.NONE);
		
		PlayerManager.games.remove(p1);
		PlayerManager.games.remove(p2);
		
		p1.removePotionEffect(PotionEffectType.REGENERATION);
		p2.removePotionEffect(PotionEffectType.REGENERATION);
		
	}
	public void endSoft(){
		a.setActive(false);
		p1.getInventory().setContents(inv1);
		p2.getInventory().setContents(inv2);
		p1.getInventory().setArmorContents(arm1);
		p2.getInventory().setArmorContents(arm2);
		
    	teleport();
		
		PlayerManager.setStatus(p1, GameState.NONE);
		PlayerManager.setStatus(p2, GameState.NONE);
		
		PlayerManager.games.remove(p1);
		PlayerManager.games.remove(p2);
		
		p1.removePotionEffect(PotionEffectType.REGENERATION);
		p2.removePotionEffect(PotionEffectType.REGENERATION);
	}
	public Arena getArena(){
		return a;
	}
	public void finish(Player winner, Player loser, int lose){
		broadcast("Spieler �a" + winner.getName() + " �7hat gegen �c" + loser.getName() + " �7gewonnen! Punkte: �a30�8:�c" + lose);
		end();
		winner.playSound(winner.getLocation(), Sound.LEVEL_UP, 1, 1);
		loser.playSound(p2.getLocation(), Sound.ENDERDRAGON_HIT, 1, 1);
	}
	public void score(Player p){
		if(p.equals(p1))
			score1++;
		if(p.equals(p2))
			score2++;
		
		String title = "";
		String subtitle;
		if(score1 > score2){
			title = "�a" + p1.getName() + " �8[�6" + score1 + "�8]";
			subtitle = "�c" + p2.getName() + " �8[�6" + score2 + "�8]";
		}else if(score2 > score1){
			title = "�a" + p2.getName() + " �8[�6" + score2 + "�8]";
			subtitle = "�c" + p1.getName() + " �8[�6" + score1 + "�8]";
		}else{
			title = "�a" + score1 + " �8: �a" + score2;
			subtitle = "�7Unentschieden";
		}
		
		Title.sendTitle(p1, 5, 70, 5, title, subtitle);
		Title.sendTitle(p2, 5, 70, 5, title, subtitle);
		
		inv();
		p1.teleport(a.getSpawn1().getLocation());
		p2.teleport(a.getSpawn2().getLocation());
		
		if(score1 >= 30)
			finish(p1, p2, score2);		
		if(score2 >= 30)
			finish(p2, p1, score1);
		
	}
	public Player getOpponent(Player p){
		if(p.equals(p1))
			return p2;
		if(p.equals(p2))
			return p1;
		return null;
	}
	public void teleport(){
		p1.teleport(loc1);
		p2.teleport(loc2);
	}
	public void inv(){
		ItemStack k = new Stack(Material.STICK).setName("�7Kn�ppel").addEnchantment(Enchantment.KNOCKBACK, 1).build();
		ItemStack b = new Stack(Material.SANDSTONE, 3, 2).build();
		
		p1.getInventory().setItem(0, k);
		p1.getInventory().setItem(1, b);
		p2.getInventory().setItem(0, k);
		p2.getInventory().setItem(1, b);
	}
}
